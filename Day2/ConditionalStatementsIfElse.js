'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.trim().split('\n').map(string => {
        return string.trim();
    });

    main();
});

function readLine() {
    return inputString[currentLine++];
}

function getGrade(score) {
    // Write your code here
    const score_map = { 0: "F", 5: "E", 10: "D", 15: "C", 20: "B", 25: "A" };
    let grade = "";
    for (let key in score_map){
        if ((Array(6).fill(parseInt(key)).map((x, y) => x + y)).includes(score)) {
            grade = score_map[key];
        }
    }
    return grade;
}



function main() {
    const score = +(readLine());

    console.log(getGrade(score));
}


