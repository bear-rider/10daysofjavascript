'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.trim().split('\n').map(string => {
        return string.trim();
    });

    main();
});

function readLine() {
    return inputString[currentLine++];
}

/*
 * Complete the vowelsAndConsonants function.
 * Print your output using 'console.log()'.
 */
const VOWELS = ['a', 'e', 'i', 'o', 'u'];

function vowelsAndConsonants(s) {
    let letters = s.split('');
    let vowels = letters.filter(letter => VOWELS.includes(letter));
    vowels.forEach(vowel => console.log(vowel));
    let consonants = letters.filter(letter => !VOWELS.includes(letter));
    consonants.forEach(consonant => console.log(consonant));
}


function main() {
    const s = readLine();

    vowelsAndConsonants(s);
}