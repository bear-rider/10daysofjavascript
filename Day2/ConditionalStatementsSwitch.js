'use strict';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString = '';
let currentLine = 0;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', _ => {
    inputString = inputString.trim().split('\n').map(string =>
        string.trim()
    );

    main();
});

function readLine() {
    return inputString[currentLine++];
}

function getLetter(s) {
    let first_letter = s.toLowerCase().charAt(0);
    // Write your code here
    if (['a', 'e', 'i', 'o', 'u'].includes(first_letter)) {
        return "A";
    }
    if (['b', 'c', 'd', 'f', 'g'].includes(first_letter)) {
        return "B";
    }
    if (['h', 'j', 'k', 'l', 'm'].includes(first_letter)) {
        return "C";
    }
    if (['n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'].includes(first_letter)) {
        return "D";
    }
}

function main() {
    const s = readLine();

    console.log(getLetter(s));
}